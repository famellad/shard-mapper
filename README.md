# shard-mapper

## Introduction

Shard mapper is a small utility to draw Minecraft in-game maps stored as .dat files inside the world folder, eventually it will allow stitching consecutive maps and inserting zoom levels within each other, allowing for high resolution maps in areas where the players have made high resolution maps available.

This application was made with the Shard server in mind, so your mileage may vary, and there may be some Shard server specific code, but most of the code should be general enough for you to understand and fork for your own needs.

## Dependencies

- Python 3.x
- pip install nbtlib
- pip install pillow

## Usage

Well, there's no finished code yet so I don't exactly know.

### Prototype directory

The prototype directory contains a bunch of very small utilities designed to test how to handle some aspects of the software, namely handling NBT files and drawing PNG images. There's a small example there showing how to grab a .dat file and turn it into a human-friendly PNG image.