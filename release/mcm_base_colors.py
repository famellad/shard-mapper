base_colors = [(255, 255, 255, 0),   # 0 Transparent
               (127, 178, 56, 255),  # 1  Grass
               (247, 233, 163, 255), # 2  Sand, Birch
               (199, 199, 199, 255), # 3  Bed Head
               (255, 0, 0, 255),     # 4  Lava, Redstone, TNT
               (160, 160, 255, 255), # 5  Ice
               (167, 167, 167, 255), # 6  Iron
               (0, 124, 0, 255),     # 7  Leaves
               (255, 255, 255, 255), # 8  White, Snow
               (164, 168, 184, 255), # 9  Clay
               (151, 109, 77, 255),  # 10 Jungle
               (112, 112, 112, 255), # 11 Cobble, Stone, Acacia
               (64, 64, 255, 255),   # 12 Water
               (143, 119, 72, 255),  # 13 Oak
               (255, 252, 245, 255), # 14 Birch, Dorite, Quartz
               (216, 127, 51, 255),  # 15 Acacia, Orange, Pumpkin
               (178, 76, 216, 255),  # 16 Purpur, Magenta
               (102, 153, 216, 255), # 17 Light Blue
               (229, 229, 51, 255),  # 18 Hay, Sponge, Yellow
               (127, 204, 25, 255),  # 19 Melon, Lime
               (242, 127, 165, 255), # 20 Pink
               (76, 76, 76, 255),    # 21 Gray
               (153, 153, 153, 255), # 22 Light Gray
               (76, 127, 153, 255),  # 23 Prismarine, Cyan
               (127, 63, 178, 255),  # 24 Mycelium, Purple
               (51, 76, 178, 255),   # 25 Blue
               (102, 76, 51, 255),   # 26 Dark Oak, Spruce, Brown
               (102, 127, 51, 255),  # 27 Green
               (153, 51, 51, 255),   # 28 Brick, Red
               (25, 25, 25, 255),    # 29 Coal, Obsidian, Black
               (250, 238, 77, 255),  # 30 Gold
               (92, 219, 213, 255),  # 31 Diamond, Prismarine
               (74, 128, 255, 255),  # 32 Lapis
               (0, 217, 58, 255),    # 33 Emerald
               (129, 86, 49, 255),   # 34 Spruce, Oak, Podzol
               (112, 2, 0, 255),     # 35 Netherrack, Nether*, Magma
               (209, 177, 161, 255), # 36 White Tct
               (159, 82, 36, 255),   # 37 Orange Tct
               (149, 87, 108, 255),  # 38 Magenta Tct
               (112, 108, 136, 255), # 39 Light Blue Tct
               (186, 133, 36, 255),  # 40 Yellow Tct
               (103, 117, 53, 255),  # 41 Lime Tct
               (160, 77, 78, 255),   # 42 Pink Tct
               (57, 41, 35, 255),    # 43 Gray Tct
               (135, 107, 98, 255),  # 44 Light Gray Tct
               (87, 92, 92, 255),    # 45 Cyan Tct
               (122, 73, 88, 255),   # 46 Purple Tct
               (76, 62, 92, 255),    # 47 Blue Tct
               (76, 50, 35, 255),    # 48 Brown Tct
               (76, 82, 42, 255),    # 49 Green Tct
               (142, 60, 46, 255),   # 50 Red Tct
               (37, 22, 16, 255)]    # 51 Black Tct
"""

base_colors = [(255, 255, 255, 0),   # 0 Transparent
               (247, 233, 163, 255),  # 1  Grass
               (247, 233, 163, 255), # 2  Sand, Birch
               (199, 199, 199, 255), # 3  Bed Head
               (247, 233, 163, 255),     # 4  Lava, Redstone, TNT
               (160, 160, 255, 255), # 5  Ice
               (150, 150, 150, 255), # 6  Iron
               (247, 233, 163, 255),     # 7  Leaves
               (247, 233, 163, 255), # 8  White, Snow
               (247, 233, 163, 255), # 9  Clay
               (247, 233, 163, 255),  # 10 Jungle
               (150, 150, 150, 255), # 11 Cobble, Stone, Acacia
               (128, 128, 255, 255),   # 12 Water
               (247, 233, 163, 255),  # 13 Oak
               (247, 233, 163, 255), # 14 Birch, Dorite, Quartz
               (247, 233, 163, 255),  # 15 Acacia, Orange, Pumpkin
               (150, 150, 150, 255),  # 16 Purpur, Magenta
               (150, 150, 150, 255), # 17 Light Blue
               (150, 150, 150, 255),  # 18 Hay, Sponge, Yellow
               (247, 233, 163, 255),  # 19 Melon, Lime
               (150, 150, 150, 255), # 20 Pink
               (150, 150, 150, 255),    # 21 Gray
               (150, 150, 150, 255), # 22 Light Gray
               (150, 150, 150, 255),  # 23 Prismarine, Cyan
               (247, 233, 163, 255),  # 24 Mycelium, Purple
               (150, 150, 150, 255),   # 25 Blue
               (150, 150, 150, 255),   # 26 Dark Oak, Spruce, Brown
               (150, 150, 150, 255),  # 27 Green
               (150, 150, 150, 255),   # 28 Brick, Red
               (150, 150, 150, 255),    # 29 Coal, Obsidian, Black
               (150, 150, 150, 255),  # 30 Gold
               (150, 150, 150, 255),  # 31 Diamond, Prismarine
               (150, 150, 150, 255),  # 32 Lapis
               (150, 150, 150, 255),    # 33 Emerald
               (247, 233, 163, 255),   # 34 Spruce, Oak, Podzol
               (150, 150, 150, 255),     # 35 Netherrack, Nether*, Magma
               (150, 150, 150, 255), # 36 White Tct
               (150, 150, 150, 255),   # 37 Orange Tct
               (150, 150, 150, 255),  # 38 Magenta Tct
               (150, 150, 150, 255), # 39 Light Blue Tct
               (150, 150, 150, 255),  # 40 Yellow Tct
               (150, 150, 150, 255),  # 41 Lime Tct
               (150, 150, 150, 255),   # 42 Pink Tct
               (150, 150, 150, 255),    # 43 Gray Tct
               (150, 150, 150, 255),  # 44 Light Gray Tct
               (150, 150, 150, 255),    # 45 Cyan Tct
               (150, 150, 150, 255),   # 46 Purple Tct
               (150, 150, 150, 255),    # 47 Blue Tct
               (150, 150, 150, 255),    # 48 Brown Tct
               (150, 150, 150, 255),    # 49 Green Tct
               (150, 150, 150, 255),   # 50 Red Tct
               (150, 150, 150, 255)]    # 51 Black Tct
"""
mc_color = [] # List that will hold the entire gamut of colors

# Function to generate all the colors
# 75% of the colors used by Minecraft are "scaled" versions
# Of the base colors, this function creates those missing colors
def gen_full_colors ():
    scale0 = 0.7#0.8#0.7059
    scale1 = 0.85#0.9#0.8827
    scale2 = 0.5#0.7#0.5294

    for i in range(0, len(base_colors)):
        for j in range(0, 4):
            if j == 0:
                mc_color.append(scale(base_colors[i], scale0))
            elif j == 1:
                mc_color.append(scale(base_colors[i], scale1))
            elif j == 2:
                mc_color.append(base_colors[i])
            else:
                mc_color.append(scale(base_colors[i], scale2))

# Auxiliary function to scale a list by a factor
def scale( tup, factor ):
    ncol = []
    for i in range( 0, 3 ):
        ncol.append(int(tup[i] * factor))
    return tuple(ncol)