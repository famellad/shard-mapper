import PIL

lores_size = 256
class mc_map:
    # Init function
    def __init__ (self, index, coords, scale, dimension, image):
        """Initialize a new map object

        Arguments
        index         : Integer representing the map index as indicated by Minecraft
        coords        : Tuple of ints representing the x, z coordinate of the map
        scale         : Integer from 0 to 4 representing the scale of the map
        image         : Pillow image containing the visual information of the map
        last_modified : Date of the last modification to the .dat file containing the map
        """
        # Variables that will be used by all maps
        self.index    = index
        self.coords   = coords
        self.scale    = scale
        self.dim      = dimension
        self.image    = image
        self.corners  = []
        # Variables that will be used by L4 maps
        self.l3_children = []
        self.l2_children = []
        self.l1_children = []
        self.l0_children = []
        # Additional variables
        base_size = 128
        c_size = base_size * pow(2, self.scale)
        self.offset = c_size / 2
        self.find_borders()

    # Function to find the borders of a map
    def find_borders(self):
        self.corners.append({"x" : self.coords["x"] - self.offset, "y" : self.coords["y"] - self.offset})
        self.corners.append({"x" : self.coords["x"] + self.offset, "y" : self.coords["y"] + self.offset})

    # This function appears to be obsolete
    def find_neighbours (self):
        pass

    def render_map(self):
        n_width  = self.image.width * pow(2, self.scale)
        n_height = self.image.height * pow(2, self.scale)

        final_image = self.image.resize((n_width, n_height))

        final_image.save("images/map_" + str(self.index) + ".png", "PNG")
        
        if self.scale == 4:
            self.image.save("images/map_" + str(self.index) + "_thumb.png", "PNG")

    def check_inside(self, c_map):
        # Check the X coordinate of corners
        if c_map.coords["x"] > self.corners[0]["x"] and c_map.coords["x"] < self.corners[1]["x"]:
            # Check the Y coordinate of corners
            if c_map.coords["y"] > self.corners[0]["y"] and c_map.coords["y"] < self.corners[1]["y"]:
                return True
        
        return False

    def insert(self, ins_map):
        if ins_map.scale == 4:
            print("[ERR] Cant insert! They're both max scale!")

        if self.check_inside(ins_map):
            #print("        Inserting " + str(ins_map.index))
            s = ins_map.scale
            if s == 0:
                self.l0_children.append(ins_map)
            elif s == 1:
                self.l1_children.append(ins_map)
            elif s == 2:
                self.l2_children.append(ins_map)
            elif s == 3:
                self.l3_children.append(ins_map)
            
            #print("[INF] " + str(ins_map.index) + " is inside " + str(self.index))

    # Aux aux function to determine the offset of a map respect to another
    def get_offset(self, ins_map):
        offset_x = ins_map.corners[0]["x"] - self.corners[0]["x"]
        offset_y = ins_map.corners[0]["y"] - self.corners[0]["y"]
        return (int(offset_x), int(offset_y))

    # Aux function to draw a map inside another
    def draw_inside(self, r_image, ins_map):
        ins_img = ins_map.image
        
        # First we resize the image to be inserted
        n_width  = ins_img.width * pow(2, ins_map.scale)
        n_height = ins_img.height * pow(2, ins_map.scale)

        ins_img = ins_img.resize((n_width, n_height))
        offset = self.get_offset(ins_map)

        for i in range(ins_img.width):
            for j in range(ins_img.height):
                pixel = ins_img.getpixel((i, j))
                
                if pixel[3] == 0:
                    continue

                r_image.putpixel((i + offset[0], j + offset[1]), pixel)


        #r_image.paste(ins_img, offset)

    # Function that renders the big brain map
    def render_big_map(self):
        print("[INF] Rendering map_" + str(self.index) + "... ", end="", flush=True)

        n_width  = self.image.width * pow(2, self.scale)
        n_height = self.image.height * pow(2, self.scale)

        final_image = self.image.resize((n_width, n_height))

        for m in self.l3_children:
            self.draw_inside(final_image, m)        
        for m in self.l2_children:
            self.draw_inside(final_image, m)
        for m in self.l1_children:
            self.draw_inside(final_image, m)
        for m in self.l0_children:
            self.draw_inside(final_image, m)        

        final_image.save("images/map_" + str(self.index) + "_hires.png", "PNG")
        final_image = final_image.resize((lores_size, lores_size), PIL.Image.LANCZOS)

        self.lores_image = final_image
        final_image.save("images/map_" + str(self.index) + "_lores.png", "PNG")

        print("Done!")