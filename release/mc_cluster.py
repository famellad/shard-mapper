from PIL import Image, ImageColor
from mc_map import *

class mc_cluster:
    def __init__ (self, id, image_dim):
        self.id           = id
        self.image_dim    = image_dim
        self.cluster_maps = []
        self.annexed      = False
        self.leftmost     = 1000000
        self.topmost      = 1000000
        self.rightmost    = -1000000
        self.botmost      = -1000000
        self.width        = 0
        self.height       = 0
        self.dimensions   = (0, 0)

    def cluster(self, n_cluster):
        maps_distance = 2048

        check_passed = False

        if self.annexed or n_cluster.annexed:
            return 0

        for m in self.cluster_maps:
            for n in n_cluster.cluster_maps:
                # Test whether maps are adjacent
                # North-south check
                if (abs(m.coords['y'] - n.coords['y']) == maps_distance) and (m.coords['x'] == n.coords['x']):
                    check_passed = True
                # East-west check
                if (abs(m.coords['x'] - n.coords['x']) == maps_distance) and (m.coords['y'] == n.coords['y']):
                    check_passed = True
                
                # If the check is passed, don't test anymore,
                # Immediately append the other cluster
                if check_passed:
                    print("        Clustering " + str(self.id) + " and " + str(n_cluster.id))
                    self.cluster_maps.extend(n_cluster.cluster_maps) #.append(n_cluster.cluster_maps)
                    n_cluster.annexed = True
                    return 1

        return 0

    def find_leftmost_topmost(self):
        for m in self.cluster_maps:
            if m.coords['x'] < self.leftmost:
                self.leftmost = m.coords['x']
            if m.coords['x'] > self.rightmost:
                self.rightmost = m.coords['x']
            if m.coords['y'] < self.topmost:
                self.topmost = m.coords['y']
            if m.coords['y'] > self.botmost:
                self.botmost = m.coords['y']
    
    def find_width_height(self):
        self.width  = int(abs(self.rightmost - self.leftmost) / 2048 + 1)
        self.height = int(abs(self.topmost - self.botmost) / 2048 + 1)
        

    def organize_cluster(self):
        self.find_leftmost_topmost()
        self.find_width_height()
        print("        C" + str(self.id) + ": (" + str(self.leftmost) + ", " + str(self.topmost) + ") of size (" + str(self.width) + ", " + str(self.height) + ")")

        self.map_matrix = []

        for i in range(self.width):
            temp_array = []
            for j in range(self.height):
                temp_array.append("")
            self.map_matrix.append(temp_array)

        for m in self.cluster_maps:
            xindex = int(abs(m.coords['x'] - self.leftmost) / 2048)
            yindex = int(abs(m.coords['y'] - self.topmost) / 2048)
            self.map_matrix[xindex][yindex] = m

    def render(self):
        final_w = self.image_dim * self.width
        final_h = self.image_dim * self.height

        cluster_image = Image.new("RGBA", (final_w, final_h))

        for i in range(self.width):
            for j in range(self.height):
                x_offset = i * self.image_dim
                y_offset = j * self.image_dim

                if isinstance(self.map_matrix[i][j], mc_map):
                    cluster_image.paste(self.map_matrix[i][j].lores_image, (x_offset, y_offset))

        cluster_image.save("cluster_" + str(self.id) + ".png", "PNG")