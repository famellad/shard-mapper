from func import *
from mc_cluster import mc_cluster
from mc_map import lores_size
# Minecraft map clustering extravaganza
# This file contains the logic that will cluster them clusters
cluster_collection = []
final_cluster_collection = []

def cluster_maps():
    # Cluster count
    c_cluster = 0
    
    # First, create one cluster per map
    for m in l4_maps:
        c = mc_cluster(c_cluster, lores_size)
        c.cluster_maps.append(m)
        cluster_collection.append(c)
        c_cluster += 1

    c_iter = 0
    managed_to_cluster = 1
    
    while(managed_to_cluster > 0):
        managed_to_cluster = 0
        print("[INF] Iteration " + str(c_iter))
        for i in range(len(cluster_collection)):
            for j in range(i + 1, len(cluster_collection)):
                # Try to cluster two clusters, and determine whether it was possible
                managed_to_cluster += cluster_collection[i].cluster(cluster_collection[j])
        c_iter += 1
        if managed_to_cluster == 0:
            print("        No clustering possible. Breaking.")
    
    for c in cluster_collection:
        if not c.annexed:
            final_cluster_collection.append(c)
            print("[INF] " + str(c.id) + " is a final cluster, containing " + str(len(c.cluster_maps)) + " map(s). Organizing.")
            c.organize_cluster()
            c.render()