from os import listdir
from PIL import Image, ImageColor
from nbtlib import File,nbt

from mcm_base_colors import *
from mc_map import mc_map

map_w = 128
map_h = map_w

whitelist_path = "whitelist"
whitelist = []

maps = []
l0_maps = []
l1_maps = []
l2_maps = []
l3_maps = []
l4_maps = []

# Function to prepare the environment to work with them maps
def init_environment():
    gen_full_colors()
    read_whitelist()

# Auxiliary function to open and parse an NBT file
def open_nbt_file(filename):
    nbt_data = nbt.load ( filename )
    return nbt_data

# Function that reads the whitelist and parses those maps
def read_whitelist():
    f = open(whitelist_path, "r")
    for line in f:
        if line[0] == "#":
            continue
        if line != "" and line != "\n":
            whitelist.append(int(line))
            c_map = mc_map_from_nbt(int(line))
            maps.append(c_map)
            m_scale  = c_map.scale

            if m_scale == 0:
                l0_maps.append(c_map)
            elif m_scale == 1:
                l1_maps.append(c_map)
            elif m_scale == 2:
                l2_maps.append(c_map)
            elif m_scale == 3:
                l3_maps.append(c_map)
            elif m_scale == 4:
                l4_maps.append(c_map)

    f.close()

    print("[INF] Maps processed: " + str(len(maps)))
    print("        Breakdown:")
    print("        L0: " + str(len(l0_maps)))
    print("        L1: " + str(len(l1_maps)))    
    print("        L2: " + str(len(l2_maps)))
    print("        L3: " + str(len(l3_maps)))
    print("        L4: " + str(len(l4_maps)))

# Function that makes a mc_map object given an map index
def mc_map_from_nbt( index ):
    op_map    = open_nbt_file("maps/map_" + str(index) + ".dat")
    coords    = {'x' : op_map.root['data']['xCenter'], 'y' : op_map.root['data']['zCenter']}
    scale     = op_map.root['data']['scale']
    dimension = op_map.root['data']['dimension']
    image     = parse_data(op_map.root['data']['colors'], mc_color)

    new_map = mc_map(index, coords, scale, dimension, image)
    return new_map

# Function that returns a list of all the map files in the map directory
def get_dat_files():
    return listdir("maps")

# Map that parses the NBT data and turns it into a human-friendly image
def parse_data( map_data, map_colors ):
    pixel_c = 0 # Pixel counter, makes life super easy
    map_image = Image.new("RGBA", (map_w, map_h), (0, 0, 0, 0))

    # Iterate through every pixel
    for i in range(0, map_w):
        for j in range(0, map_h):
            color_index = map_data[pixel_c]
            # If the pixel is transparent, don't set it
            #print(color_index)
            if color_index == 0:
                pixel_c += 1
            else:
                map_image.putpixel((j, i), map_colors[color_index])
                pixel_c += 1

    return map_image

# Function that prints a test image with all the base colors
def print_test_image():
    img = Image.new("RGBA", (16, 16))
    counter = 0

    for i in range(16):
        for j in range(16):
            img.putpixel((j, i), base_colors[counter])
            counter += 1
            if counter == len(base_colors):
                break
        if counter == len(base_colors):
            break
    
    img.save("test.png", "PNG")

# Function that prints a test image with the full gamut of colors
def print_full_test_image():
    img = Image.new("RGBA", (16, 16))
    counter = 0

    for i in range(16):
        for j in range(16):
            img.putpixel((j, i), mc_color[counter])
            counter += 1
            if counter == len(mc_color):
                break
        if counter == len(mc_color):
            break
    
    img.save("full_test.png", "PNG")