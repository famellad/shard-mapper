import sys

from func import *
from clusterer import *
#map_paths = get_dat_files()

init_environment()

mmax = float(len(l4_maps))
mcur = 0.0

for m in l4_maps:
    #print("{:.2f}%".format(mcur*100.0/mmax))
    mcur += 1

    # Only process maps in the overworld
    if m.dim != 0:
        continue
    
    for s_map in l3_maps:
        m.insert(s_map)
    for s_map in l2_maps:
        m.insert(s_map)
    for s_map in l1_maps:
        m.insert(s_map)
    for s_map in l0_maps:
        m.insert(s_map)

    m.render_big_map()
    #maps.append(cmap)

cluster_maps()

# TODO Stitch maps together
