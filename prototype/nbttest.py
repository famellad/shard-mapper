# Example file on how to open and access NBT data inside a .dat file
from nbtlib import File,nbt

# It is opened like any regular file
map_file = nbt.load("map_0.dat")

# Paths inside the NBT are accessed like this, this points towards /data/colors
colarray = map_file.root['data']['colors']

for i in range(0, len(colarray)):
    print( colarray[i] )
