import sys
from nbtlib import File,nbt
from PIL import Image, ImageColor

from mcm_base_colors import base_colors

filename = sys.argv[1]

w = 128 # Width of the map
h = 128 # Height of the map

color = [] # List that will hold the entire gamut of colors

# Function to generate all the colors
# 75% of the colors used by Minecraft are "scaled" versions
# Of the base colors, this function creates those missing colors
def gen_full_colors ():
    for i in range(0, len(base_colors)):
        for j in range(0, 4):
            if j == 0:
                color.append(scale(base_colors[i], 180/255))
            elif j == 1:
                color.append(scale(base_colors[i], 220/255))
            elif j == 2:
                color.append(base_colors[i])
            else:
                color.append(scale(base_colors[i], 135/255))

# Auxiliary function to scale a list by a factor
def scale( tup, factor ):
    ncol = []
    for i in range( 0, 3 ):
        ncol.append(int(tup[i] * factor))
    return tuple(ncol)

# Auxiliary function to open an NBT file
def open_file ( filename ):
    nbt_data = nbt.load ( filename )
    return nbt_data

# Generate all the colors
gen_full_colors()

# Open the map file
test_map = open_file(filename + ".dat")

# Extract the color data from the map, located at /data/colors
colarray = test_map.root['data']['colors']

pixel_c = 0 # Pixel counter to make my life easy

# Create a new PILlow image
test_img = Image.new("RGB", (w, h))

# Translate the color data to the newly created image
for i in range(0, w):
    for j in range(0, h):
        test_img.putpixel((j, i), color[colarray[pixel_c]])
        pixel_c += 1

#test_img = test_img.rotate(90)

# Save the image
test_img.save(filename + ".png", "PNG")