# This piece of code was made to exemplify how to draw an image using PIL, pixel by pixel

from PIL import Image, ImageColor

# The image is created, mode and dimensions are required
w = 512
h = 512
test_img = Image.new("RGB", (w, h))

# Visit and draw every pixel individually, putpixel receives two tuples as arguments
# The first one is the coordinate, and the second one is the RGB colour
for i in range(0, w):
    for j in range(0, h):
        test_img.putpixel((i,j), ((int(i/2), int((i+j)/4), int(j/2))))

# Save the image, that's pretty much it
test_img.save("test_img.png", "PNG")
